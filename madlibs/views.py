import random
from django.shortcuts import render, redirect
from django.urls import reverse
from madlibs.forms import StoryForm, PoemForm

def Main(request):
    return render(request, "madlibs/list.html")

def StoryInput(request, template=None):
    story_templates = [
        'story1.html',
        'story2.html',
        'story3.html',
        'story4.html',
        'story5.html',
        'story6.html',
        'story7.html',
        'story8.html',
        'story9.html',
        'story10.html',
    ]

    if request.method == 'POST':
        form = StoryForm(request.POST)
        if form.is_valid():
            cleaned_data = form.cleaned_data
            random_story_template = random.choice(story_templates)
            context = {
                "template": random_story_template,
                "form": form,
                **cleaned_data
            }
            return render(request, f"madlibs/{random_story_template}", context)
    else:
        form = StoryForm()

    context = {
        "form": form,
    }
    return render(request, "madlibs/storyUserInput.html", context)


def PoemInput(request):

    poem_templates = [
        'poem1.html',
        'poem2.html',
        'poem3.html',
        'poem4.html',
        'poem5.html',
    ]

    if request.method == 'POST':
        form = PoemForm(request.POST)
        if form.is_valid():
            cleaned_data = form.cleaned_data
            random_poem_template = random.choice(poem_templates)
            context = {
                "template": random_poem_template,
                "form": form,
                **cleaned_data
            }
            return render(request, f"madlibs/{random_poem_template}", context)
    else:
        form = PoemForm()

    context = {
        "form": form,
    }
    return render(request, "madlibs/poemUserInput.html", context)