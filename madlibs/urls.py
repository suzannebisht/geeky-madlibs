from django.urls import path
from madlibs.views import StoryInput, PoemInput, Main

urlpatterns = [
    path('', Main, name='main'),
    path('story/', StoryInput, name='storyinput'),
    path('poem/', PoemInput, name='poeminput'),
    path('story/<str:template>/', StoryInput, name='randomstory'),
]