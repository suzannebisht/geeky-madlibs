from django import forms

class SignUpForm(forms.Form):
    name = forms.CharField(max_length=200)
    email = forms.EmailField(max_length=250)
    username = forms.CharField(max_length=100)
    password = forms.CharField(
        max_length=100,
        widget=forms.PasswordInput,
    )
    passwordConfirmation = forms.CharField(
        max_length=100,
        widget=forms.PasswordInput,
    )